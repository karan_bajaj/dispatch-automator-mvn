package com.rivigo.finance;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import java.awt.print.PrinterJob;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

public class DispatchAutomator {

    private static final String PRINTER_NAME = "4th floor finance color printer";

    private static final String SEP = OsUtils.isWindows()? "\\":"/";

    private static final String COMPASS_PATH = System.getProperty("user.home")+SEP+"Downloads"+SEP+"Compass";

    public static void main(String[] args) {

        List<File> zipFiles = findFilesInDirectory(COMPASS_PATH, "zip");

        for (File zipFile : zipFiles) {

            String fileName = zipFile.getName();
            String fileNameWithoutExtension = fileName.substring(0, fileName.lastIndexOf("."));

            String destDir = COMPASS_PATH + SEP + "Unzipped" + SEP + fileNameWithoutExtension;

            unzip(zipFile.toString(), destDir);

            List<File> pdfList = findFilesInDirectory(destDir, "pdf");

            String customerAddress = null, customerName = null;
            for(File pdf : pdfList)    {

                /*
                // Get customer address and name from Invoice
                if (pdf.toString().contains("INVOICE")) {
                    customerAddress = getInvoiceAddress(pdf);
                    customerName = customerAddress.split("\n")[0];
                    //System.out.println(customerAddress);

                    // Write address to post file
                    writeToFile(COMPASS_PATH + SEP + "ToPost.txt", customerAddress);
                }
                */

                // Print the invoice and annexure
                //printFile(pdf);
            }
            //movePOD(destDir, customerName);

            deleteFolder(destDir);
            deleteFolder(COMPASS_PATH + SEP + "Unzipped");
        }
    }

    private static void deleteFolder(String folderPath) {

        System.out.println("[Delete] "+folderPath);

        File dir = new File(folderPath);
        String[]entries = dir.list();
        for(String s: entries){
            File currentFile = new File(dir.getPath(),s);
            currentFile.delete();
        }
        dir.delete();
    }

    private static void movePOD(String destDir, String customerName)    {

        System.out.println("[Move] POD for " + customerName);

        try {
            String targetPathDir = COMPASS_PATH + SEP + customerName;
            String targetPath = targetPathDir + SEP + "POD.zip";
            new File(targetPathDir).mkdirs();
            Files.move(Paths.get(destDir + SEP + "POD.zip"),
                    Paths.get(targetPath));
        }
        catch(Exception e)  {
            System.out.println("[ERR] Cannot move POD due to exception - "+e);
        }
    }

    private static void writeToFile(String filename, String content)   {

        System.out.println("Writing to file " + filename);

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true));
            writer.append(content);
            writer.append("\n\n");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getInvoiceAddress(File file)    {

        System.out.println("[FIND] Address from " + file.getName());

        String address = "";
        String lines[] = readPDFLines(file);

        List<String> linesArray = Arrays.asList(lines);

        boolean bRecord = false;

        for(String line : linesArray)   {
            if(line.contains("HSN/SAC Code : 9965"))    {
                bRecord = true;
            }
            else if(bRecord) {
                if(line.contains("Place of Supply"))
                    break;
                if(!address.isEmpty())
                    address+= "\n";
                address += line;
            }
        }
        return address;
    }

    private static String[] readPDFLines(File file) {

        System.out.println("[Read] PDF: "+file.getName());

        String lines[] = null;
        try {
            PDDocument document = PDDocument.load(file);

            document.getClass();

            if (!document.isEncrypted()) {

                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition(true);

                PDFTextStripper tStripper = new PDFTextStripper();

                String pdfFileInText = tStripper.getText(document);
                //System.out.println("PDF text:" + pdfFileInText);

                lines = pdfFileInText.split("\\r?\\n");
            }
        }
        catch (Exception e) {
            System.out.println("PDF parsing failed with exception: ("+e+") for file "+file.toString());
        }
        return lines;
    }

    private static void printFile(File file)   {
        System.out.println("[Print] "+file.getName());
        try {
            PDDocument document = PDDocument.load(file);
            PrintService myPrintService = findPrintService(PRINTER_NAME);

            PrinterJob job = PrinterJob.getPrinterJob();
            job.setPageable(new PDFPageable(document));
            job.setPrintService(myPrintService);
            job.print();
        }
        catch(Exception e)    {
            System.out.println("[ERR] Printing failed with exception: ("+e+") for file "+file.toString());
        }
    }

    private static PrintService findPrintService(String printerName) {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        for (PrintService printService : printServices) {
            if (printService.getName().trim().equals(printerName)) {
                return printService;
            }
        }
        return printServices[0];
    }

    private static List<File> findFilesInDirectory(String directory, final String extn)    {

        System.out.println("[FIND] " + extn + " in " + directory);

        File f = new File(directory);
        File[] matchingFiles = f.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(extn);
            }
        });
        return Arrays.asList(matchingFiles);
    }

    private static void unzip(String zipFilePath, String destDir) {

        System.out.println("[UNZIP] " + zipFilePath);

        File dir = new File(destDir);
        if(!dir.exists()) dir.mkdirs();
        FileInputStream fis;
        byte[] buffer = new byte[1024];
        try {
            fis = new FileInputStream(zipFilePath);
            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry ze = zis.getNextEntry();
            while(ze != null){
                String fileName = ze.getName();
                File newFile = new File(destDir + File.separator + fileName);
                new File(newFile.getParent()).mkdirs();
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
                zis.closeEntry();
                ze = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

class OsUtils
{
    private static String OS = null;
    public static String getOsName()
    {
        if(OS == null) { OS = System.getProperty("os.name"); }
        return OS;
    }
    public static boolean isWindows()
    {
        return getOsName().startsWith("Windows");
    }
}